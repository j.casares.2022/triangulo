#Escribir un programa que acepte como argumento (en línea de comandos) un número, y muestre un triángulo de
#números de la forma siguiente: primero una línea con un 1, luego una línea con dos 2, luego una línea con tres 3,
#y así hasta una línea que tenga número veces número.
import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    return str(number) * number


def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    cadena = ""
    if number < 10:
        try:
            for num in range(1, number+1):
                x = line(number)
                cadena = cadena+x+"\n"
        except ValueError:
            print("prueba otro numero del 1 al 9")
    return cadena


def main():
    number: str = sys.argv[1]
    text = triangle(int(number))
    print(text)


if __name__ == '__main__':
    main()
